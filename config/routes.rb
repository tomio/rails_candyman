Rails.application.routes.draw do

  namespace :api do
    resources :drivers  do 
      resources :items, only: [:index,:create]
      resources :orders
    end 

    resources :users 

    resources :orderables 

    put 'drivers/:driver_id/items/:orderable_id', to: "items#update"
    put 'users', to: "users#update"
    put 'update_payment', to: "users#update_payment"
    post 'set_delivered', to: "drivers#set_order_delivered"
    post 'update_location',to: "drivers#update_location"
    post 'update_inventory', to: "drivers#update_inventory"
    get 'delete_pending_orders', to: "orders#delete_all_pending_orders"
    get 'order_still_exist', to: "orders#order_still_exist"
    get 'order_exists', to: "users#has_existing_order"
    post 'add_promotion', to: "orders#add_promotion"
    post 'delete_pending_order', to: "orders#delete_pending_order"
    post 'add_gcm_id', to: "users#add_gcm_id"
    post 'create_user', to: "users#create_user"
    post 'update_address', to: "users#update_address"

    get 'validate_address', to: "users#validate_address"
    get 'drivers/:driver_id/items/:orderable_id', to: "items#show"
    get 'next_order', to: "drivers#next_order"
    get 'remaining_time', to: "users#get_remaining_time"
    post 'orders', to: "users#orders"
    post 'account', to: "users#account_info"
    delete 'drivers/:driver_id/items/:orderable_id', to: "items#destroy"
    post 'new_orders/new_order', to: "drivers#assign_order"
    post "confirm/:id", to: "orders#confirm"
    post  "login", to: "users#login"

  end


  resources :orderables, only: [:new,:create,:index,:edit,:update]  








  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
