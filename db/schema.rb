# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160903021843) do

  create_table "apps", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.decimal  "price",                     precision: 10
    t.text     "description", limit: 65535
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string   "zip",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "drivers", force: :cascade do |t|
    t.string   "location",        limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "delivery_time",   limit: 4,   default: 0
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.boolean  "admin",                       default: false
    t.string   "gcm_id",          limit: 255
  end

  create_table "items", force: :cascade do |t|
    t.integer  "driver_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "orderable_id", limit: 4
    t.integer  "quantity",     limit: 4
  end

  add_index "items", ["orderable_id"], name: "index_items_on_orderable_id", using: :btree

  create_table "orderables", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.decimal  "price",                          precision: 8, scale: 2
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
  end

  create_table "orders", force: :cascade do |t|
    t.string   "location",   limit: 255
    t.string   "info",       limit: 255
    t.integer  "driver_id",  limit: 4
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.integer  "state",      limit: 4,                           default: 0
    t.integer  "user_id",    limit: 4
    t.decimal  "total",                  precision: 8, scale: 2
  end

  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "phones", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.integer  "battery",          limit: 4
    t.integer  "speed_difference", limit: 4
    t.integer  "overall",          limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.string   "code",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "used_promotions", force: :cascade do |t|
    t.integer  "promotion_id", limit: 4
    t.integer  "user_id",      limit: 4
    t.integer  "order_id",     limit: 4
    t.integer  "state",        limit: 4, default: 0
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "address",         limit: 255
    t.string   "stripe_id",       limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.boolean  "tester",                      default: false
    t.string   "gcm_id",          limit: 255
  end

  add_foreign_key "items", "orderables"
  add_foreign_key "orders", "users"
end
