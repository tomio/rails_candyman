class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :code 
      t.string :description
      t.timestamps null: false
    end
  end
end
