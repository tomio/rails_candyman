class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :location 
      t.string :info
      t.references :driver
      t.timestamps null: false
    end	
  end
end
