class AddAdminColumnToDrivers < ActiveRecord::Migration
  def change
  	  	add_column :drivers, :admin, :boolean,  default: false

  end
end
