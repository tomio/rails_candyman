class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :quantity
      t.string :name 
      t.decimal :price 
      t.references :driver
      t.timestamps null: false
    end
  end
end
