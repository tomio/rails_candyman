class CreateOrderables < ActiveRecord::Migration
  def change
    create_table :orderables do |t|

      t.string :name
      t.integer :quantity
      t.decimal :price
      t.timestamps null: false
    end
  end
end
