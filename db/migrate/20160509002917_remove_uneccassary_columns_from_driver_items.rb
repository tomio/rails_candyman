class RemoveUneccassaryColumnsFromDriverItems < ActiveRecord::Migration
  def change
  	remove_column :items,:quantity, :integer
  	remove_column :items,:name,  :string
  	remove_column :items,:price,  :decimal
  	add_reference :items, :orderable, index: true, foreign_key: true


  end
end
