class AddTotalToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :total, :decimal, :precision => 8, :scale => 2
  	change_column :orderables, :price, :decimal, :precision => 8, :scale => 2

  end
end
