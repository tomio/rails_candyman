class CreateUsedPromotions < ActiveRecord::Migration
  def change
    create_table :used_promotions do |t|
      t.references :promotion
      t.references :user
      t.references :order

      t.integer :state, default: 0
      t.timestamps null: false
    end
  end
end
