

desc 'update task '
task update_inventory_task: :environment do
  # ... set options if any
  Driver.delete_pending_orders
end

task send_can_order_notification: :environment do
  # ... set options if any
  User.notify_can_order
end