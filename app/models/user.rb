class User < ActiveRecord::Base
	has_secure_password 

	has_many :orders
	has_many :used_promotions

    validates :password_confirmation, presence: false	
    validates :email, uniqueness: true
    validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
	validates :password, length: { minimum: 6 }

	def valid_attribute?(attribute_name)
  		self.valid?
  		self.errors[attribute_name].blank?
	end

	def card_info 

		if self.tester 
			key = "sk_test_9TSHx41FPbp6qvjqlEBvoJbL"
		else 
			key = "sk_live_d0mYCw2Q2KdI0tRMgOQLYFaD"
		end 
		Stripe.api_key = key



		if self.stripe_id != nil 
			return Stripe::Customer.retrieve(self.stripe_id.to_s).sources.data.to_json
		else 
			return "new"
		end 

	end 	

	def notify_order_delivered
		server_key = "AIzaSyCKGrDt0IXBx25ZRepKwnwcxK2Faw8SDSk"
		
		uri = URI("https://fcm.googleapis.com/fcm/send")
		req = 	Net::HTTP::Post.new	(uri)
		req.content_type = "application/json"
		req["Authorization"] = "key="+server_key
		req.body = '{"data":{"message": "Your order has arrived	"}, "registration_ids" :["'+self.gcm_id.to_s+'"]}'
		req["Content-Type"] = "application/json"

		res = Net::HTTP.start(uri.hostname, 80) do |http|
  			http.request(req)
  		end	
  		Rails.logger.info(req.body)

  		Rails.logger.info(res.body)

	end 

	def self.send_notifcation(message,gcm_ids) 
		server_key = "AIzaSyCKGrDt0IXBx25ZRepKwnwcxK2Faw8SDSk"

		uri = URI("https://fcm.googleapis.com/fcm/send")
		req = 	Net::HTTP::Post.new	(uri)
		req.content_type = "application/json"
		req["Authorization"] = "key="+server_key
		req.body = '{"data":{"message": "'+message+'"}, "registration_ids" :['+gcm_ids+']}'
		req["Content-Type"] = "application/json"
		res = Net::HTTP.start(uri.hostname, 80) do |http|
  			http.request(req)
  		end	

  		Rails.logger.info(req.body)

  		Rails.logger.info(res.body)


	end 

	#notify every user that they can order 
	def self.notify_can_order 

		users = User.where.not(gcm_id: nil)
		registration_ids = ''
		
		users.each do |user|
			if users.last == user 
				registration_ids+='"'+user.gcm_id+'"'
			else 
				registration_ids+= '"'+user.gcm_id+'",'
			end 
		end 

		message = "It's 6pm! You can order pizza now!"

		send_notifcation(message,registration_ids)

		

	end 	

		 


end
