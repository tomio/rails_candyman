class Order < ActiveRecord::Base
	belongs_to :driver 
	belongs_to :user
	has_one :used_promotion

	def add_promotion(promotion)
		case promotion.code 
		
			when "FIRST"
				total = (self.total/2).round(2) 
				self.update_attribute(:total,total)	

				#number_with_precision(or, :precision => 2)
		end
		user = self.user
		used_promo = user.used_promotions.where(promotion_id: promotion.id).first 
		self.used_promotion = used_promo 
	end 		

end
