class Driver < ActiveRecord::Base
	has_secure_password 

	has_many :items
	has_many :orders 
	

	def notify_new_order 
		
		server_key = "AIzaSyCKGrDt0IXBx25ZRepKwnwcxK2Faw8SDSk"
		
		uri = URI("https://fcm.googleapis.com/fcm/send")
		req = 	Net::HTTP::Post.new	(uri)
		req.content_type = "application/json"
		req["Authorization"] = "key="+server_key
		req.body = '{"data":{"message": "A new order has been placed"}, "registration_ids" :["'+self.gcm_id.to_s+'"]}'
		req["Content-Type"] = "application/json"

		res = Net::HTTP.start(uri.hostname, 80) do |http|
  			http.request(req)
  		end	
  		


	end 

	def add_to_items_inventory(info)
		

	   	order_info = info.split(",")
		
		(0.. (order_info.count-1 ) ).step(2) do |i|
			
			orderable_id = order_info[i]
			quantity = order_info[i+1]

			item =self.items.where(orderable_id: orderable_id).first
			current_quantity = item.quantity
			new_quantity = current_quantity + quantity.to_i 
			item.update_attribute(:quantity,new_quantity)
		end 


		

	end 

	def self.d 
		Order.all.count 
	end 



	def self.delete_pending_orders 
	    Rails.logger.info("CitrixIndex updated at #{Time.now}")

		orders = Order.where(state: 0)
		
		if orders.count > 0 	
		   # loop through each order and restore the inventory 
		   # of each driver restoring the order 

		   orders.each do |order| 

		   	time = Time.now 
		   	difference = time - order.created_at 
		   	
		   	# the order is too old
		   	too_old = difference > (60 * 2) 
		   
		   	if !too_old 
			   	driver = order.driver
				driver.add_to_items_inventory(order.info)
			end

			if too_old 
				order.destroy
			end 

			end 	

		end

	end 

end 
