class Api::ItemsController < Api::ApiController 
		before_filter :get_driver 


	def index 
		items = @driver.items.all
		render status: 200, json: 
		{
			items: items, 
			message: "The items have been retrieved"
		}.to_json
	end 

	def show 
		item = @driver.items.find_by(orderable_id: params[:orderable_id])

		render json: 
		{
			item: item
		}.to_json

	end 	

	def update 
		item = @driver.items.find_by(orderable_id: params[:orderable_id])
		
		if item.update(item_params)
			render status: 200, json: 
			{
				message: "successfully updated item", 
				item: item 
			}.to_json

		else 
			render status: 422, json: 
			{
				message: "unsuccessfully updated item"

			}.to_json
		end 



	end 	

	def destroy 	
			
			item = @driver.items.find_by(orderable_id: params[:orderable_id])
			if item.destroy
				render status: 200, json: 
				{
					message: "successfully deleted item "

				}.to_json
			end 
			

	end 


	def create 
		item = @driver.items.new(item_params)
		if item.save 

			render status: 200,
			json: 
			{ 

				message: "The item was successfully added",
				item: item
			 
			}.to_json

		else 

			render status: 422,
			json: 
			{
				message: "there was an issue adding the item"
			 
			}.to_json

		end 

	end 



	private 

	def get_driver
		#@ makes it accesible freom anywhere inside contorller 
		@driver = Driver.find(params[:driver_id])
	end 

	def item_params
		params.require("item").permit(:orderable_id,:quantity)
	end 	

end 
