module Api
	class ApiController < ApplicationController 
		skip_before_filter :verify_authenticity_token
		protect_from_forgery with: :null_session
		before_filter :authenticate
		def authenticate 
			authenticate_or_request_with_http_basic do|email,password|
				user = User.find_by(email: email)
				verified =  user != nil && user.authenticate(password)
				if verified
					@user = user 
					@user_id = user.id

				else 
					driver = Driver.find_by(email: email)
					verified = driver != nil && driver.authenticate(password)
					if verified
						@driver = driver
					end 
				end 
				verified
			end 
		end

		
		  



	end 

end 