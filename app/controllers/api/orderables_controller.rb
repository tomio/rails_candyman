class Api::OrderablesController < Api::ApiController 
		skip_before_filter :verify_authenticity_token

	def index 
		orderables = Orderable.all
		json = []
		json.push({time: Time.new.hour})
		orderables.each do |orderable| 
		
		if orderable.image_file_name != nil 
			image = Base64.encode64(File.open(orderable.image.path(:small),"rb") { |io| io.read })
		else 
			image = nil 
		end 
		json.push({

			id: orderable.id,
			name: orderable.name,
			price: orderable.price, 
			image: image

			})

		end 

		render status: 200, json: json.as_json

	end 		

	def create 
		orderable = Orderable.new(orderable_params)
		if orderable.save 
		   render status: 200, json: 
		   {
		   	orderable: orderable, 
		   	message: "succuessfully saved orderable"
		   }.to_json
		else
		render status: 422, json: 
		   {
		   	message: "unsuccuessfully saved orderable"
		   }.to_json

		end 


	end 

	private 

	def orderable_params 
		
		params.require(:orderable).permit(:name,:price) 

	end 
	



end 