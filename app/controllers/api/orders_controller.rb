class Api::OrdersController < Api::ApiController 


	def index 
		render json: @driver.orders.all.as_json 
	end 

	# delete all orders that have not been confirmed in
	# the last five minutes

	def delete_pending_order 
		order = @user.orders.find_by_id(params[:id])

		if order != nil
			message = "Nothing to delete "

			if order.state == 0 
				driver = order.driver 
				#order.used_promotion.order = nil 
				
				driver.add_to_items_inventory(order.info)
				order.destroy
				message = "Order deleted"

			end 
			render status: 200, json: 
			{
				message: message
			}.to_json

		else 
			render status: 422, json: 
			{
				message: "The order was not found "
			}.to_json 

		end 



	end 

	
	def add_promotion

		promotion = Promotion.where(code: params[:code])
		order = @user.orders.find_by_id(params[:order_id])


		if promotion.count > 0

			promotion = promotion.first # get the actual object 
			#check if the promotion has been used 
			used_promotion = @user.used_promotions.where(promotion_id: promotion.id) 
				
			if used_promotion.count == 0
				
				@user.used_promotions.create(promotion_id: promotion.id)
				
				if order != nil 
					order.add_promotion(promotion) 

					render status: 200, json:  
					{
						total: order.total,
						accepted: "true",
						message: promotion.description
					}.to_json
				else 
					render status: 200, json: 
					{
						accepted: "expired", 
						message: "The order has expired"
					}.to_json


				end 

			else	
				#the promotion has been used before 
				used_promotion = used_promotion.first # get the object 
				if used_promotion.state == 0 
					#the promotion has just been added to the account and not used
					message = "Promotion already added to account"

				else 
					#the promotion has been used 
					message = "Promotion already used"

				end 

					render status: 200, json: 
					{
						accepted: "used",
						message: message
					}.to_json


			end 
		else 
			render status: 422, json: 
			{
				accepted: false, 
				message: "Promotion not found "
			}.to_json

		end 



	end 	

	def order_still_exist 
		
		if @user.orders.find_by_id(params[:id]) != nil 
			render status: 200, json: 
			{
				exist: true ,
				message: "Order still exist"
			}.to_json
		else 
			render status: 200, json: 
			{
				exist: false ,
				message: "Order does not exist"
			}.to_json
		end
	end 



	def delete_all_pending_orders 
		#Driver.delete_pending_orders
		render status: 200, json: 
		{
			count: Driver.d 
		}.to_json



	end 	

	def confirm 
			#check if the user is a test account 

			if @user.tester 
				key = "sk_test_9TSHx41FPbp6qvjqlEBvoJbL"
			else 
				key = "sk_live_d0mYCw2Q2KdI0tRMgOQLYFaD"
			end 

		  Stripe.api_key = key 

  	@user = User.find(@user_id)
  	if params.has_key?(:stripeToken)

				token = params[:stripeToken]
				customer_email = 	@user.email 
				
				begin
				
				order = Order.find(params[:id])

			  customer = Stripe::Customer.create(
			    :email => customer_email,
			    :source  => token
			  )

			  charge = Stripe::Charge.create(
			    :customer    => customer.id,
			    :amount      => (order.total * 100).to_i,
			    :description => @user.email,
			    :currency    => 'usd'
			  )

			  @user.update_attribute(:stripe_id, customer.id)

				Rails.logger.info(@user.errors.inspect) 


			  update_order_status(params[:id])



			rescue Stripe::CardError => e
		 	 # The card has been declined

		 	 render status: 422, json: 

		 	 {
		 	 	success: false,
		 	 	message: e.message
		 	 }.to_json


			end

  	else 

  		begin 
		
		order = Order.find(params[:id])

  		charge = Stripe::Charge.create(
			    :customer    => @user.stripe_id,
			    :amount      => (order.total * 100).to_i,
			    :description => @user.email,
			    :currency    => 'usd'
			  )

  		update_order_status(params[:id])


  		rescue Stripe::CardError => e 	
		 	 render status: 422, json: 

		 	 {
		 	 	success: false,
		 	 	message: e.message
		 	 }.to_json
  		end 


		end 

	end 

	private 

	


	def update_order_status(order_id)
		order = Order.find(order_id)

			if order.used_promotion != nil 
				order.used_promotion.update_attribute(:state,1)
			end 	
	

		 if order.update(state: 1)
		 	order.driver.notify_new_order
		 	render status: 200, json: 
		 	{ 
		 		success: true,
		 		message: "The order has been successfully updated " , 
		 		orders: Order.find(order_id)
		 	}.to_json 

		 else	 
		 	render status: 422, json: 
		 	{
		 		message: "The order has been unsuccessfully updated " 
		 	}.to_json 

		 end 


	end 

	def order_params 
		params.require(:order).permit(:info,:driver_id,:location,:user_id)
	end 

end 