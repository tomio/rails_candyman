class Api::UsersController < Api::ApiController 
	
	skip_before_filter :authenticate, :only => [:login,:create,:create_user]

	def index 
	
		render json: User.all.as_json
	end 

	def has_existing_order 
		
		has_order = @user.orders.where(state: 1).count > 0 
		
		render json: 
		{
		  exist: has_order
		}.to_json

	end


	def show 
  	user = User.find(@user_id)
  	render json: user.orders.as_json
	end 

	def orders 
	 user = User.find_by(email: params[:email])

	 if user && user.authenticate(params[:password])
			
			orders = user.orders.where(["state = ?", "1"])
			if orders.count < 1 
				render status: 200, json: 
				{
					has_orders: -1, 
					message: "the user has no active orders"
				}
			else 

			orders_info = [] 
			orders.each do |order| 
				
				items = order.info.split(",")
				items_info = []
				(0.. (items.count-1) ).step(2) do |i|
					item_name = Orderable.find(items[i]).name
					quantity = items[i+1]
					items_info.push(quantity+" "+item_name )

				end 

				order_info = {

				delivery_time: "3 min",
				items: items_info

				}

				orders_info.push(order_info)

			end 
			render status: 200, json: 
			{
				has_orders: 1,
				orders: orders_info

			}.to_json
		end 

		else 

			render status: 200, json: 
			{
				message: "there was a problem authenticating the user"
			}.to_json

		end 

	end 

	def account_info 

	 user = User.find(@user_id)

	 	if user

		 render status: 200, json: 
		 {
				email: user.email, 
				address: user.address, 
				card: user.card_info
		 }.to_json

	else 

		render status: 422, json: 
		{
			message: "could not retrieve user info"
		}.to_json

	end 

	end 


	def get_remaining_time

		
		user = User.find(@user_id)
		
		order_id = params[:order_id]	
		if order_id != nil 
			user_order = user.orders.find_by_id(order_id)
		else 
			user_order = user.orders.where(state: 1).order("updated_at DESC").first
		end 

		if user_order == nil 

			render status: 422, json: 
			{ 
				order_id: -1, 
				state: -1, 
				time: - 1	
			}.to_json

		elsif user_order.state == 2 
			render status: 200, json: 
			{
				order_id: user_order.id, 
				state: user_order.state, 
				time: 1000
			}.to_json 
		

		 
		else

			driver = user_order.driver

			driver_orders = driver.orders.where(state: 1).order("updated_at ASC")

			#go through each of the drivers orders and do 
			#a summation of each of the drivers location from each 
			#delivery locatino and the location in question

			time = 0 # the amount of time it will take for the driver 
			previous_location = ""
			locations = []
			
			driver_orders.each do |order|
			
			if order.id == user_order.id && previous_location == ""
				time+= get_distance_between(driver.location,order.user.address)
				locations.push(driver.location)
				locations.push(order.user.address)
				break

			elsif previous_location == "" && order.id != user_order.id 
				time+= get_distance_between(driver.location,order.user.address)
				previous_location = order.user.address
				locations.push(driver.location)
				locations.push(order.user.address)
				Rails.logger.info("true") 

			elsif order.id == user_order.id
				time += get_distance_between(previous_location,order.user.address)
				locations.push(previous_location)
				locations.push(order.user.address)
				Rails.logger.info("brue") 

				break
			
			else 
				time+= get_distance_between(previous_location,order.user.address)
				locations.push(previous_location)
				locations.push(order.user.address)
				previous_location = order.user.address 

			end 
			
			end 


			render status: 200, json: 
			{
				order_id: user_order.id, 
				state: user_order.state,
				time: time
			}.to_json
		end 
	end 
	def login 
		user = User.find_by(email: params[:email])
		driver = Driver.find_by(email: params[:email])
		password = params[:password]
		if user && user.authenticate(password)

			render status: 200, json: 
			{
				type: "customer", 
				id: user.id,
				tester: user.tester,
				address: user.address

			}.to_json

		elsif driver && driver.authenticate(password)

			render status: 200, json: 
			{
				type: "driver", 
				id: driver.id
			}.to_json
					

		else 

			render status: 422,json: 
			{
				id: -1, 
				message: "user was not found"

			}.to_json
			
		end 

		end
	
	def update 
		
		user = User.find(@user_id) 			
	  
	  	email = params[:new_email]
			address = params[:address]
			message = []
			status = 200
			#user is trying to update emai 
			if user.email != email 
				user.email = email

				if user.valid_attribute?(:email)  

					 user.update_attribute(:email,email)
					 message.push ("E-mail successfully updated")
						
				else 
					status = 422
					Rails.logger.info(user.errors.inspect) 
					message.push("E-mail is invalid")

				end 
			end 
			
			if user.address != address 
				validation_message,time = check_each_city(address)


				if validation_message == "FOUND"
					user.update_attribute(:address,address)
					message.push("Address successfully updated")
				elsif validation_message == "TOO_FAR"
					status = 422
					message.push("PizzaCar not offered at the address input. Ensure the full address is entered")
				else 
					status = 422
					message.("Address not found")
				end 


			end 


				render status: status, json: 
				{
					message: message, 
					user: user
				}.to_json

	end

	def add_gcm_id 
		gcm_id = params[:gcm_id]
		updated = false 
		if @user
			updated = true  
			@user.update_attribute(:gcm_id,gcm_id)
		else @driver 
			updated = true 
			@driver.update_attribute(:gcm_id,gcm_id)
		end 

		render json: 
		{
			message: updated
		}.to_json

	end 

	def update_payment 
	  Stripe.api_key = "sk_live_d0mYCw2Q2KdI0tRMgOQLYFaD"

		user = User.find(@user_id)

			#get stripe customer profile 
			
			if user.stripe_id != nil 
				customer = Stripe::Customer.retrieve(user.stripe_id)

				card_info = customer.sources.all(:object => "card")

				#the user has a card already set in account 
				
				if card_info["data"].count > 0 
					card_id = card_info["data"][0]["id"]
					customer.sources.retrieve(card_id).delete()
				end 

				begin 
						
					customer.sources.create(:source => params[:stripe_token])
					card_info = customer.sources.all(:object => "card")

					render status: 200, json: 
					{  	success: true,
						customer: card_info
					}.to_json

				rescue Stripe::CardError => e
				 	 # The card has been declined

				 	 	render status: 422, json: 
					 	 {  success: false,
					 	 	message: e.message
					 	 }.to_json
				end 

				else 

						
					begin
				
					  customer = Stripe::Customer.create(
					    :email => user.email,
					    :source  => params["stripe_token"]
					  )

					  user.update_attribute(:stripe_id, customer.id)

						Rails.logger.info(user.errors.inspect) 

						render status: 200, json: 
						{
							success: true,
							d: user.stripe_id,
							card: user.card_info
						}.to_json

					rescue Stripe::CardError => e
				 	 # The card has been declined

				 	 	render status: 422, json: 
					 	 {
					 	 	success: false,
					 	 	message: e.message
					 	 }.to_json

					end
			end 
	end 

	def validate_address
		address = params[:validate_address]
		validity,time = check_each_city(address)
		render json: 
		{ 
			 	message: validity,
			 	time: time

		}.to_json

	end 

	def create_user 

		user = User.new(user_params)

				if user.save
				render json: 
				{
					status: "200",
					message: "the user has been saved",
				user: user
			}.to_json
		else 

			if user.errors.any?	
				message = user.errors.full_messages.first
			else 
				message = "fail"
			end 
			render status: 422, json: 
			{

				user: {id: -1} ,
				message: message
			 
			}.to_json	

		end  

	end 


	def update_address 

	address = params[:address]
	address_status,time= check_each_city(address)

	address_is_valid = true 

	if address_status == "TOO_FAR"
		address_is_valid = false 
		message = "PizzaCar is not offered at the address input. Ensure the full address is entered"
	elsif address_status == "NOT_FOUND"
		message = "The address entered was not found. Please type in the full address"
		address_is_valid = false 

	end 	


		if address_is_valid && @user.update_attribute(:address,address)
			render status: 200,json: 
			{
				message: "the user has been saved",
				user: @user
			}.to_json
		else 
			render status: 422, json: 
			{
				address_status: address_status,
				message: message
			}.to_json	

		end
	end 		

	def create

	user = User.new(user_params)
	message,time= check_each_city(params[:user][:address])
	Rails.logger.info(message)
	address_is_valid = true 

	if message == "TOO_FAR"
		address_is_valid = false 
		message = "PizzaCar is not offered at the address input. Ensure the full address is entered"
	elsif message == "NOT_FOUND"
		message = "The address entered was not found. Please type in the full address"
		address_is_valid = false 

	end 	


		if address_is_valid && user.save
			render json: 
			{
				status: "200",
				message: "the user has been saved",
				user: user
			}.to_json
		else 

			if user.errors.any?	
				message = user.errors.full_messages.first
			end 
			render json: 
			{
				status: "500",
				user: {id: -1},
				message: [message]
			 
			}.to_json	

		end  
		
	end

	private  

	def user_params
		params.require("user").permit(:address,:email,:password,:stripe_id)
	end 

	def check_each_city(address)
		validity = ""
		time = -1
		City.all.each do |city|
			validity,time= check_address_validity(address,city.zip)

			case  
		
				when "FOUND"
					break
				when "TOO_FAR"

				when "NOT_FOUND"
					break 
			end 
		end 

		return validity,time 
	end 
	def check_address_validity(address,city_zip)
		base_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+city_zip
		base_url += "&destinations="+address		
		result = Net::HTTP.get(URI.parse(base_url))
		parsed_json = ActiveSupport::JSON.decode(result)

		#time it would take to get to address 
		#from dispactch location 
		duration = parsed_json["rows"][0]["elements"][0]["duration"]
		time = nil 
		if duration != nil 

			time = duration["value"]
			if time < (60 * 30) # everything within a thirty minutes radius of the validate_address
				message ="FOUND"
			else  					
				message =  "TOO_FAR"
			end 
		else
			message = "NOT_FOUND"
		end 

		return message,time 
	end 	

	 def skip_password_attribute
    if params[:password].blank?
      params.except!(:password)
    end
  end

  def get_distance_between(origin,destination)
  	base_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origin
		base_url += "&destinations="+destination
		api_key = "AIzaSyCma7yOGAngxdf7ZPaKkfm22S1WFTTT7QI"
		base_url+="&key="+api_key		
		result = Net::HTTP.get(URI.parse(base_url))
		parsed_json = ActiveSupport::JSON.decode(result)
		return parsed_json["rows"][0]["elements"][0]["duration"]["value"]

  end 

end