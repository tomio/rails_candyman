class Api::DriversController < Api::ApiController 
require 'net/http'

	skip_before_filter :authenticate, :only => :create

	def index 
		render json: Driver.all.as_json(include:[:items,:orders]) 

	end 

	def show 
		driver = Driver.find(params[:id])
	render json: driver.as_json(include:[:items,:orders])

	end 


	def create 
		if Driver.find_by(email: "tomiokiji@gmail.com")
			render status: 200, json: 
			{
				message: "driver already created"
			}
		else 

			driver = Driver.new(email: "tomiokiji@gmail.com",password: "nintendo")
			if driver.save
				render json: 
				{
					status: "200",
					message: "the driver has been saved",
					driver: driver
				}.to_json
			else 
				render json: 
				{
					status: "500",
					message: "there was an issue saving the driver"
				 
				}.to_json	

			end 
		end

	end 

	def assign_order 
	

		order_info  = params["order_info"] 
		user_id = @user.id
		destination = @user.address

		base_url,driver_ids,delivery_times = format_url(destination)

		result = Net::HTTP.get(URI.parse(base_url))
		parsed_json = ActiveSupport::JSON.decode(result)
		

		#driver,order_delivery_time,time_text= find_driver_with_shortest_time(parsed_json,order_info,driver_ids,delivery_times)
		driver = Driver.first
		if driver != nil && driver_has_enough(driver,order_info)
			time_text = (estimate_delivery_time(driver)/60).to_s+ " mins"
			total = calculate_total_price(order_info)
			order = driver.orders.create(total: total,location: destination, info: order_info, user_id: user_id)
			#Driver.find(driver.id).update(delivery_time: (driver.delivery_time + order_delivery_time))

			#check if the user has a promotion to add 
			promo = "null"

			promotions = @user.used_promotions.where(state: 0)
			if promotions.count > 0
				promo = promotions.first.promotion
				order.add_promotion(promo)
			end 

			items = fetch_items_info(order_info)
			user = User.find(user_id)
			render status: 200, json: 

			{ 


				promotion: promo,
				card: user.card_info,
				delivery_time: time_text,
				driver_id: driver.id, 
				order_id: order.id,
				order: order,
				items: items,
				message: "the driver for the order has been found"


			}.to_json 

		else 
			
			render status: 422, json: 

			{


				message: "the driver for the order has been not found",
				driver_id: -1, 
				order_id: -1


			}.to_json 


		end 




	end 	


	def update 

		if @driver.update(driver_params)
			render json: 
			{
				status: "200",
				message: "the driver has been updated"
			}.to_json


		else 

			render status: 422,
			json: 
			{
				message: "there was an issue updating the driver"
			 
			}.to_json	


		end 
	end 

	def update_inventory

		inventory_string = params[:inventory]
		updated_inventory = []
		times = 0
		updated_fully = true  
		if inventory_string != nil 

			inventory_updates_array = inventory_string.split(",")
			(0.. (inventory_updates_array.count-1 ) ).step(2) do |i|
				times+=1
				orderable_id = inventory_updates_array[i]
				quantity = inventory_updates_array[i+1]

				if !change_inventory(@driver,orderable_id,quantity)
					updated_fully = false 
				else 
					updated_inventory.push({orderable_id: orderable_id,quantity: quantity})
				end 
			end 

		end 
		if updated_fully
			render status: 200, json: 
			{
				message: "Updated", 
				items: updated_inventory
			}.to_json
		else 
			render status: 422, json: 
			{
				message: "The update info provided was incorrect"
			}.to_json 
		end 



	end 

	def update_location 
		new_location = params[:location]
		
		if new_location != nil && @driver.update_attribute(:location,new_location)
			render status: 200, json: 
			{
				message: "location successfully updated",
				driver: @driver.location
			}.to_json
		else 
			render status: 422, json: 
			{
				message: "unable to update location "
			}.to_json		
		end 


	end 

	def set_order_delivered 
		order = @driver.orders.find(params[:order_id])


		if order.state != 2 
			order.user.notify_order_delivered

			order.update_attribute(:state,2) #order just delivered
			user = order.user 
			locs = calculate_delivery_time()
			render json: 
			{
				order: order
			}.to_json  
		else 
			render status: 422, json: 
			{
				message: "already updated"
			}.to_json

		end 	
		
	end 		


	def destroy 
		driver = Driver.find(params[:id])
		driver.destroy
		render json: 
			{
				status: "200",
				message: "the driver has been destoryed"
				}.to_json

	end 


	def next_order 
		order = @driver.orders.where(state: 1).order("updated_at ASC").first

		if order != nil


			 		items = order.info.split(",")
				items_info = []
				(0.. (items.count-1) ).step(2) do |i|
					item_name = Orderable.find(items[i]).name
					quantity = items[i+1]
					items_info.push({quantity:quantity,item_name:item_name} )

				end 
			render status: 200, json: 
			{
				items_info: items_info,
				order: order,
				orders_left: @driver.orders.where(state: 1).count-1 
			}.to_json
		else 

			render status: 200, json: 
			{
				order:{}
			}.to_json
		end 

	end 

	private 

	def estimate_delivery_time(driver) 
			driver_orders = driver.orders.where(state: 1).order("updated_at ASC")

			#go through each of the drivers orders and do 
			#a summation of each of the drivers location from each 
			#delivery locatino and the location in question

			time = 0 # the amount of time it will take for the driver 
			previous_location = ""
			locations = []


			driver_orders.each do |order|
				
				if previous_location == "" 
					time+= get_distance_between(driver.location,order.user.address)
					locations.push(driver.location)
					locations.push(order.user.address)

				else 
					time+= get_distance_between(previous_location,order.user.address)
					locations.push(previous_location)
					locations.push(order.user.address)

				end 

				previous_location = order.user.address	
			
			end 

		if previous_location == "" 
			previous_location = driver.location
		end 
		
		time+= get_distance_between(previous_location,@user.address)
		time.round
	end 	

	def find_driver_with_shortest_time(json,order_info,driver_ids,delivery_times)


		shortest_time = nil # shortest time of the driver delivering 
		driver_id = nil # the id of the driver chosen 
		order_delivery_time = nil # the delivery time with of the order 
		time_text =nil # the simplified delivery time to show to user 
		potential_drivers = {} # just in case the driver chosen does not enough items on hand  
		j = 0

		json["rows"].each do |row| 
		 	
		 	time_row = row["elements"][0]["duration"]
		 	time = time_row["value"]
		 	time_text = time_row["text"]

		 	current_driver_id = driver_ids[j]
		 	delivery_time = delivery_times[driver_ids[j]]

		 	if  delivery_time == nil 
		 		delivery_time = 0
		 	end 

		 	total_time = delivery_time + time 
		 	potential_drivers[current_driver_id] = total_time

			if shortest_time == nil 
				shortest_time = total_time 
				driver_id = current_driver_id
				order_delivery_time = time 
			elsif shortest_time > total_time
				shortest_time = total_time
				driver_id = current_driver_id
				order_delivery_time = time 

			end 

			j+=1

		 end 

		driver = Driver.find(driver_id)
		if !driver_has_enough(driver,order_info)
			# sort the times from greatest to least 
			potential_drivers = potential_drivers.sort {|a,b| a[1]<=>b[1]} 
			potential_drivers.each do |key, array|
  		driver = Driver.find(key)
  		if driver_has_enough(driver,order_info)
  			order_delivery_time = array -driver.delivery_time
  			break 
  		else 
  			driver = nil 
  	  end 

		end 

		end 
 
		return driver,order_delivery_time,time_text
	end 





	def driver_params

		params.require("driver").permit(:location,:delivery_time)
	end 

	def fetch_items_info(order_info)

		orderables = []
		orders_list = order_info.split(",")
		(0.. (orders_list.count-1) 	).step(2) do |i|

			orderable = Orderable.find(orders_list[i])
			json = 
			{
      			id: orderable.id,
      			name: orderable.name,
      			price: orderable.price,
      			quantity: orders_list[i+1]
			}

			orderables.push(json)
		end 

		

		return orderables
	end 

	def driver_has_enough(driver,info)

		info_list = info.split(",")
		updated_inventory = Hash.new 

		has_enough = true 
		(0.. (info_list.count-1)).step(2) do |i|


			orderable_id = info_list[i].to_i
    	quantity = info_list[i+1].to_i
    	item = driver.items.where(orderable_id: orderable_id).first

  		if item == nil || item.quantity < quantity

  			has_enough = false 
  			break 

    		else 
    			updated_inventory[item.orderable_id] = item.quantity - quantity
    		end 

		end


		if has_enough
			updated_inventory.each do |orderable_id,quantity|
				is_it = change_inventory(driver,orderable_id,quantity)
				if is_it 
					Rails.logger.info("Got it" +updated_inventory.count.to_s)
				else 
					Rails.logger.info("This is " +orderable_id.to_s)
				end  

			end 
		end

		has_enough


	end 




	def change_inventory(driver,orderable_id,new_quantity) 
		
		fully_updated = true 

				
			item = driver.items.where(orderable_id: orderable_id).first
			
			if item
					item.update_attribute(:quantity, new_quantity )
			elsif Orderable.where(id: orderable_id).first != nil  
					driver.items.create({orderable_id: orderable_id, quantity: new_quantity})
			else 
				fully_updated = false 
			end 	

		fully_updated

	end 	


	def format_url(destination)
		driver_ids = []
		delivery_times = Hash.new 


		api_key = "AIzaSyCma7yOGAngxdf7ZPaKkfm22S1WFTTT7QI"

		
		base_url = "https://maps.googleapis.com/maps/api/distancematrix/json?"

		base_url+="origins="
		i = 0 # help keep track of the driver ids  

		delivery_times = Hash.new 
		
		Driver.all.each do |driver|
		
			last_order = driver.orders.last #most recent order to driver 
			
			if last_order == nil 
				location = driver.location
			else 
				location = last_order.location
				delivery_times[driver.id] = driver.delivery_time

			end 

			if i == 0 
				base_url+= driver.location
				i+=1 
			else 
				base_url+="|"+driver.location
			end 

			driver_ids.push(driver.id)


		end 
		
		base_url+="&destinations="+destination

		base_url+="&key="+api_key

		return base_url,driver_ids,delivery_times

	end 

	def calculate_delivery_time
		orders = @driver.orders.where(state: 1).order("updated_at ASC")

		time = 0
		previous_location = nil
		locations = []

		orders.each do |order| 


			if orders.first.id = order.id 
				# order 
					time+= get_distance_between(@driver.location,order.location)
					locations.push(@driver.location)
					locations.push(order.location)
			else 
					time+= get_distance_between(previous_location,order.location)
					locations.push(previous_location)
					locations.push(order.location)
			end 
					previous_location = order.location

		end 

		@driver.update_attribute(:delivery_time,time)
		return locations

	end 	

	def get_distance_between(origin,destination)
  	base_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origin
		base_url += "&destinations="+destination
		api_key = "AIzaSyCma7yOGAngxdf7ZPaKkfm22S1WFTTT7QI"
		base_url+="&key="+api_key		
		result = Net::HTTP.get(URI.parse(base_url))
		parsed_json = ActiveSupport::JSON.decode(result)
		return parsed_json["rows"][0]["elements"][0]["duration"]["value"]

  end 

  def calculate_total_price(order_info) 
		
		info = order_info.split(",")
		total = 0
		(0.. (info.count-1) ).step(2) do |i|
				total+= Orderable.find(info[i]).price * info[i+1].to_i
			end 

		total

  end 	

	 
end 



