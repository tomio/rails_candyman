class OrderablesController < ApplicationController
before_filter :authenticate

def index 
	@orderables = Orderable.all

end 	

def create 

	if @driver == nil || @driver.admin == false 
		render json: {
			message: "not allowed to go"
		}
	end 	

	@orderable = Orderable.new(orderable_params)
	if @orderable.save
		redirect_to "/api/orderables"
	else 
		render action: :new 
	end 
end 

def new 
	@orderable = Orderable.new 

end 

def edit 

	if @driver == nil || @driver.admin == false 
		render json: {
			message: "not allowed to go"
		}
	end 


	@orderable = Orderable.find(params[:id])

end 

def update 
	@orderable = Orderable.find(params[:id])
		if @orderable.update_attributes(orderable_params)
			@orderables = Orderable.all
			render action: :index
		else 
			flash[:error] = "There was a problem updating the tea"
			render action: :edit 
		end 
end 



 

private 

def orderable_params 
		params.require(:orderable).permit(:name,:price,:image)
end 

def authenticate 
			authenticate_or_request_with_http_basic do|email,password|
				user = User.find_by(email: email)
				verified =  user != nil && user.authenticate(password)
				if verified
					@user_id = user.id
				else 
					driver = Driver.find_by(email: email)
					verified = driver != nil && driver.authenticate(password)
					if verified
						@driver = driver
					end 
				end 
				verified
			end 

end 

end 