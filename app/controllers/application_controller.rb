class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  def render_404
  	respond_to do |format|
  		format.html do 
  			render file: 'public/404.html', status: :not_found, layout: :false   			
  		end 

  		format.json do 
  			render status: 404, json: 
  			{
  				message: "not found"
  			}.to_json
  		end 	

  	end 

  end 
end
